# SkinSim auto experimenter

# About
This will enable automatic configuring and experimenting on the SkinSim platform.

# Run
- Generate an experimental model configuration
```
./genExpSpecs
```
- Automatically run the generated model config generated at skinsim_test/config/mdlSpecs.yaml
```
./automatedTest
```

The experimental data will be saved to data/efc_(Ne)_(sens_rad)_(space_wid).dat

